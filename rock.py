#!/usr/bin/env python3
"""
project: rock-scissors-paper-game
file: rock.py
version: 1.00
summary: the game of Rock Scissors Paper
"""
import random

OPTIONS_COUNT: int = 5
ROCK, SCISSORS, PAPER, QUIT, UNKNOWN = range(OPTIONS_COUNT)
RULES: list = ["Scissors beats Paper", "Paper beats rock", "Rock beats Scissors"]
SELECTION: list = ["Rock", "Scissors", "Paper", "Quit", "Unknown"]
GAMES_COUNT: int = -1
STAT_USER_WIN: int = 0
STAT_COMP_WIN: int = 0
STAT_TIE: int = 0
STAT_ROCK_WIN: int = 0
STAT_SCIS_WIN: int = 0
STAT_PAPER_WIN: int = 0

def say_hello():
    """ Print greeter message """
    print("\nWelcome to the Rock-Scissors-Paper game.")
    print(f"\nRules: {RULES}")

def say_bye():
    """ Print goodbye message """
    print("\nGood bye from the Rock-Scissors-Paper game!\n")

def get_user_choice():
    """ Get users choice based on the input """
    user_input = input("> ").lower()
    switcher = {"r": SELECTION[ROCK],
                "rock": SELECTION[ROCK],
                "s": SELECTION[SCISSORS],
                "scissors": SELECTION[SCISSORS],
                "p": SELECTION[PAPER],
                "paper": SELECTION[PAPER],
                "q" : SELECTION[QUIT],
                "quit": SELECTION[QUIT]}
    user_selection = switcher.get(user_input, SELECTION[UNKNOWN])

    return user_selection

def pick_winner(user_selection, computer_selection):
    """ Pick a winner """

    global STAT_USER_WIN, STAT_COMP_WIN, STAT_TIE, STAT_ROCK_WIN, STAT_SCIS_WIN, STAT_PAPER_WIN

    print(f"{user_selection=} {computer_selection=}")

    if user_selection == computer_selection:
        print("Tie!")
        STAT_TIE += 1
        return

    if user_selection == SELECTION[SCISSORS]:
        if computer_selection == SELECTION[PAPER]:
            print(f"Computer got Paper -> {RULES[0]} (Rule #1) -> you win!")
            STAT_USER_WIN += 1
            STAT_SCIS_WIN += 1
        elif computer_selection == SELECTION[ROCK]:
            print(f"Computer got Rock -> {RULES[2]} (Rule #3) -> you lose!")
            STAT_COMP_WIN += 1
            STAT_ROCK_WIN += 1
        else:
            print("Unknown result!")

    elif user_selection == SELECTION[PAPER]:
        if computer_selection == SELECTION[SCISSORS]:
            print(f"Computer got Scissors -> {RULES[0]} (Rule #1) -> you lose!")
            STAT_COMP_WIN += 1
            STAT_SCIS_WIN += 1
        elif computer_selection == SELECTION[ROCK]:
            print(f"Computer got Rock -> {RULES[1]} (Rule #2) -> you win!")
            STAT_USER_WIN += 1
            STAT_PAPER_WIN += 1
        else:
            print("Unknown result!")

    elif user_selection == SELECTION[ROCK]:
        if computer_selection == SELECTION[SCISSORS]:
            print(f"Computer got Scissors -> {RULES[2]} (Rule #3) -> you win!")
            STAT_USER_WIN += 1
            STAT_ROCK_WIN += 1
        elif computer_selection == SELECTION[PAPER]:
            print(f"Computer got Paper -> {RULES[1]} (Rule #2) -> you lose!")
            STAT_COMP_WIN += 1
            STAT_PAPER_WIN += 1
        else:
            print("Unknown result!")

def show_stats():
    """ Show some statistics """
    global GAMES_COUNT, STAT_USER_WIN, STAT_COMP_WIN, STAT_TIE, STAT_ROCK_WIN, STAT_SCIS_WIN,\
        STAT_PAPER_WIN
    print(f"\nShow some statistics.")
    print(f" # Games: {GAMES_COUNT}")
    print(f" You won: {STAT_USER_WIN} Computer won: {STAT_COMP_WIN} Ties: {STAT_TIE}")
    print(f"Rock won: {STAT_ROCK_WIN} Scissors won: {STAT_SCIS_WIN} Paper won: {STAT_PAPER_WIN}")

def play_game():
    """ Repeat the playing-game loop """
    global GAMES_COUNT
    user_selection: str = ""
    while user_selection != SELECTION[QUIT]:
        print("\nWhat do you want to play?")
        print("[R]ock [S]cissors [P]aper [Q]uit game")

        user_selection = get_user_choice()

        computer_selection = random.choice(SELECTION[ROCK:QUIT])

        if user_selection == SELECTION[UNKNOWN]:
            print("\nUnknown selection! Please type r/s/p or q.")
        else:
            GAMES_COUNT += 1
            pick_winner(user_selection, computer_selection)

def main():
    """ Define the main program """
    say_hello()
    play_game()
    show_stats()
    say_bye()

if __name__ == '__main__':
    main()
