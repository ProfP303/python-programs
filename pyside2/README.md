Hi, I am having troubles to run my compiled windows-executable built with pyinstaller.

I am using the PySide2 python bindings for Qt.

I have designed some .Ui files with Qt Designer and loading those during run-time, e.g:

    from PySide2.QtUiTools import loadUiType, QUiLoader
    from PySide2.QtWidgets import QApplication, QMainWindow

    class MainWindow(QMainWindow, loadUiType("win_main.ui")[0]):
        def __init__(self):
            super().__init__()
            
            self.setupUi(self)
This runs fine.

Also the created exe-file runs fine on my machine.

But when I deploy the .exe file to any other machine the exe crashes with following error:

    Cannot run 'pyside2-uic':  "Process failed to start"
    Check if 'pyside2-uic' is in PATH
    Traceback (most recent call last):
      File "main.py", line 12, in <module>

The mentioned line 12 is the one loading the Ui-File:

    class MainWindow(QMainWindow, loadUiType("win_main.ui")[0]):

- File "main.py" is the runnable main python program
- File "win_main.ui" is the Ui-File created in Qt-Designer which is being loaded in line 12.
- File "pyinstaller_main.bat" installs all necessary modules and creates the Windows-Executable.
