@ECHO OFF

pip install --upgrade pip
pip install --upgrade wheel
pip install --upgrade pyside2
pip install --upgrade pyinstaller

pyside2-uic win_main.ui > ui_win_main2.py
pyinstaller main2.py --name=main2 --clean --noconfirm --console --onedir --log-level=ERROR --hidden-import=PySide2.QtXml

CALL .\dist\main2\main2.exe

PAUSE
