# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'win_main.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_winMain(object):
    def setupUi(self, winMain):
        if not winMain.objectName():
            winMain.setObjectName(u"winMain")
        winMain.resize(1550, 720)
        winMain.setMinimumSize(QSize(1550, 720))
        self.centralwidget = QWidget(winMain)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_2 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.splitter = QSplitter(self.centralwidget)
        self.splitter.setObjectName(u"splitter")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.splitter.sizePolicy().hasHeightForWidth())
        self.splitter.setSizePolicy(sizePolicy)
        self.splitter.setOrientation(Qt.Horizontal)
        self.grpNavigation = QGroupBox(self.splitter)
        self.grpNavigation.setObjectName(u"grpNavigation")
        self.verticalLayout = QVBoxLayout(self.grpNavigation)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.treeWidget = QTreeWidget(self.grpNavigation)
        __qtreewidgetitem = QTreeWidgetItem(self.treeWidget)
        QTreeWidgetItem(__qtreewidgetitem)
        __qtreewidgetitem1 = QTreeWidgetItem(self.treeWidget)
        QTreeWidgetItem(__qtreewidgetitem1)
        __qtreewidgetitem2 = QTreeWidgetItem(self.treeWidget)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        self.treeWidget.setObjectName(u"treeWidget")
        self.treeWidget.header().setVisible(False)

        self.verticalLayout.addWidget(self.treeWidget)

        self.splitter.addWidget(self.grpNavigation)
        self.grpView = QGroupBox(self.splitter)
        self.grpView.setObjectName(u"grpView")
        self.verticalLayout_2 = QVBoxLayout(self.grpView)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.layoutView = QVBoxLayout()
        self.layoutView.setObjectName(u"layoutView")

        self.verticalLayout_2.addLayout(self.layoutView)

        self.splitter.addWidget(self.grpView)

        self.horizontalLayout_2.addWidget(self.splitter)

        winMain.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(winMain)
        self.statusbar.setObjectName(u"statusbar")
        self.statusbar.setToolTipDuration(3)
        winMain.setStatusBar(self.statusbar)
        self.menubar = QMenuBar(winMain)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1550, 23))
        self.menubar.setToolTipDuration(3)
        self.menuProMon = QMenu(self.menubar)
        self.menuProMon.setObjectName(u"menuProMon")
        self.menuProMon.setToolTipsVisible(True)
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        self.menuHelp.setToolTipsVisible(True)
        self.menuSettings = QMenu(self.menubar)
        self.menuSettings.setObjectName(u"menuSettings")
        winMain.setMenuBar(self.menubar)

        self.menubar.addAction(self.menuProMon.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuProMon.addSeparator()

        self.retranslateUi(winMain)

        QMetaObject.connectSlotsByName(winMain)
    # setupUi

    def retranslateUi(self, winMain):
        winMain.setWindowTitle(QCoreApplication.translate("winMain", u"Provider Monitor", None))
        self.grpNavigation.setTitle(QCoreApplication.translate("winMain", u"Navigation", None))
        ___qtreewidgetitem = self.treeWidget.headerItem()
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("winMain", u"Navigation", None));

        __sortingEnabled = self.treeWidget.isSortingEnabled()
        self.treeWidget.setSortingEnabled(False)
        ___qtreewidgetitem1 = self.treeWidget.topLevelItem(0)
        ___qtreewidgetitem1.setText(0, QCoreApplication.translate("winMain", u"Node1", None));
        ___qtreewidgetitem2 = ___qtreewidgetitem1.child(0)
        ___qtreewidgetitem2.setText(0, QCoreApplication.translate("winMain", u"Item1", None));
        ___qtreewidgetitem3 = self.treeWidget.topLevelItem(1)
        ___qtreewidgetitem3.setText(0, QCoreApplication.translate("winMain", u"Node2", None));
        ___qtreewidgetitem4 = ___qtreewidgetitem3.child(0)
        ___qtreewidgetitem4.setText(0, QCoreApplication.translate("winMain", u"Item2", None));
        ___qtreewidgetitem5 = self.treeWidget.topLevelItem(2)
        ___qtreewidgetitem5.setText(0, QCoreApplication.translate("winMain", u"Node3", None));
        ___qtreewidgetitem6 = ___qtreewidgetitem5.child(0)
        ___qtreewidgetitem6.setText(0, QCoreApplication.translate("winMain", u"Item3", None));
        ___qtreewidgetitem7 = ___qtreewidgetitem5.child(1)
        ___qtreewidgetitem7.setText(0, QCoreApplication.translate("winMain", u"Item4", None));
        self.treeWidget.setSortingEnabled(__sortingEnabled)

        self.grpView.setTitle(QCoreApplication.translate("winMain", u"View", None))
        self.menuProMon.setTitle(QCoreApplication.translate("winMain", u"MyApp", None))
        self.menuHelp.setTitle(QCoreApplication.translate("winMain", u"Help", None))
        self.menuSettings.setTitle(QCoreApplication.translate("winMain", u"Settings", None))
    # retranslateUi

