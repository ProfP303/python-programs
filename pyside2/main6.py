#!/usr/bin/env python3
"""
project: pyside6-sample
file: main6.py
summary: class definition for main gui program
"""

import sys
from PySide6.QtUiTools import loadUiType, QUiLoader
from PySide6.QtWidgets import QApplication, QMainWindow
from ui_win_main6 import Ui_winMain

# class MainWindow(QMainWindow, loadUiType("win_main.ui")[0]):
# pyside6-uic win_main.ui > ui_win_main6.py
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_winMain()
        self.ui.setupUi(self)

if __name__ == "__main__":
    app = QApplication(sys.argv)

    winMain = MainWindow()
    winMain.show()
    
    sys.exit(app.exec_())
