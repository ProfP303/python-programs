@ECHO OFF

::Use Virtual env
CALL .\env\Scripts\activate.bat

pip install --upgrade pyside6
pip install --upgrade pyinstaller
pip list

SET QT_DEBUG_PLUGINS=1
pyside6-uic win_main.ui > ui_win_main6.py
pyinstaller main6.py --name=main6 --clean --noconfirm --console  --onedir  --log-level=ERROR --hidden-import=PySide6.QtXml

::ONE DIR VERSION
::copy /Y %LOCALAPPDATA%\Programs\Python\Python39\Lib\site-packages\PySide6\plugins\platforms .\dist\main6
mkdir .\dist\main6\platforms
::copy /Y %LOCALAPPDATA%\Programs\Python\Python39\Lib\site-packages\PySide6\plugins\platforms .\dist\main6\platforms
copy /Y .\env\Lib\site-packages\PySide6\plugins\platforms .\dist\main6\platforms

::ONE FILE VERSION
pyinstaller main6.py --name=main6 --clean --noconfirm --windowed --onefile --log-level=ERROR --hidden-import=PySide6.QtXml
mkdir .\dist\platforms
copy /Y .\env\Lib\site-packages\PySide6\plugins\platforms .\dist\platforms

RENAME %LOCALAPPDATA%\Programs\Python\Python37 _Python37
RENAME %LOCALAPPDATA%\Programs\Python\Python39 _Python39

::Disable Virtual env
CALL .\env\Scripts\deactivate.bat

CALL .\dist\main6\main6.exe

RENAME %LOCALAPPDATA%\Programs\Python\_Python37 Python37
RENAME %LOCALAPPDATA%\Programs\Python\_Python39 Python39



