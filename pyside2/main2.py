#!/usr/bin/env python3
"""
project: pyside2-sample
file: main.py
summary: class definition for main gui program
"""

import sys
from PySide2.QtUiTools import loadUiType, QUiLoader
from PySide2.QtWidgets import QApplication, QMainWindow
from ui_win_main2 import Ui_winMain

# class MainWindow(QMainWindow, loadUiType("win_main.ui")[0]):
# pyside2-uic win_main.ui > ui_win_main2.py
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_winMain()
        self.ui.setupUi(self)

if __name__ == "__main__":
    app = QApplication(sys.argv)

    winMain = MainWindow()
    winMain.show()
    
    sys.exit(app.exec_())
