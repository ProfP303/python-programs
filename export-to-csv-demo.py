#!/usr/bin/env python3

"""
project: SQLite/CSV examples
file: export-to-csv-demo.py
summary: demonstrates exporting the result of a SQLite query to a csv-file
author: paul salajean (https://gitlab.com/ProfP303/)
license: MIT
"""


import sqlite3
import csv  # see: https://docs.python.org/3/library/csv.html#csv.writer

connection = sqlite3.connect(":memory:")
cursor = connection.cursor()

# Create table 'logs'
sql_command = """
CREATE TABLE logs (time text, kind text, message text, jid_id int);"""
cursor.execute(sql_command)

# Insert some data into table 'logs'
sql_command = """
INSERT INTO logs (time, kind, message, jid_id) VALUES
(date('now'), 'D', 'This is my 1st msg.', 1),
(date('now'), 'D', 'This is my 2nd msg.', 2),
(date('now'), 'D', 'Here we go.', 3);"""
cursor.execute(sql_command)


# Select from table 'logs'
sql_command = """
SELECT time, kind, message, jid_id FROM logs WHERE jid_id > 0;"""
result = cursor.execute(sql_command)

# fetch all data from table 'logs' into a list
data = []
for row in result.fetchall():
    print(row)
    data.append(row)

# write content of list into a csv-file
with open("export_logs2.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(data)

