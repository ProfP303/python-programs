"""
project: SQLite/JSon example
file: json-demo.py
summary: demonstrates storing/loading a JSON-string
author: paul salajean (https://gitlab.com/ProfP303/)
license: MIT
"""

import json

def store():
    """Storing a JSON string."""
    str_json: str = '{"0": "Very unlikely, here we go", "1": "e.g. once in 5-10 years", \
        "2": "Unlikely", "3": "e.g. once in 2-5 years", "4": "Possible"}'

    return str_json

def load(str_json):
    """Loading a JSON string."""
    str_json = json.loads(str_json)

    for i in range(len(str_json)):
        print(str_json[str(i)])

# main
STR_JSON = store()
load(STR_JSON)
