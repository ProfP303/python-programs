#!/usr/bin/env python3
"""
project: rock-scissors-paper-game
file: rock_oop.py
version: 1.00
summary: the game of Rock Scissors Paper in OOP paradigm
"""
# pylint: disable=line-too-long

import random

OPTIONS_COUNT: int = 5
ROCK, SCISSORS, PAPER, QUIT, UNKNOWN = range(OPTIONS_COUNT)
RULES: list = ["Scissors beats Paper", "Paper beats rock", "Rock beats Scissors"]
SELECTION: list = ["Rock", "Scissors", "Paper", "Quit", "Unknown"]

def say_hello():
    """ Print greeter message """
    print("\nWelcome to the Rock-Scissors-Paper game.")
    print(f"\nRules: {RULES}")

def say_bye():
    """ Print goodbye message """
    print("\nGood bye from the Rock-Scissors-Paper game!\n")

class Game():
    """ Implement the rock-scissors-paper-game """
    def __init__(self):
        self.games_count: int = 0
        self.user_win: int = 0
        self.comp_win: int = 0
        self.tie: int = 0
        self.rock_win: int = 0
        self.scis_win: int = 0
        self.paper_win: int = 0

    def get_user_choice(self):
        """ Get users choice based on the input """
        user_input = input("> ").lower()
        switcher = {"r": SELECTION[ROCK],
                    "rock": SELECTION[ROCK],
                    "s": SELECTION[SCISSORS],
                    "scissors": SELECTION[SCISSORS],
                    "p": SELECTION[PAPER],
                    "paper": SELECTION[PAPER],
                    "q" : SELECTION[QUIT],
                    "quit": SELECTION[QUIT]}
        user_selection = switcher.get(user_input, SELECTION[UNKNOWN])

        return user_selection

    def pick_winner(self, user_selection, computer_selection):
        """ Pick a winner """
        print(f"Computer selected {computer_selection}.")

        if user_selection == computer_selection:
            print("Tie!")
            self.tie += 1
            return

        if user_selection == SELECTION[SCISSORS]:
            if computer_selection == SELECTION[PAPER]:
                print(f"{RULES[0]} (Rule #1) -> you win!")
                self.user_win += 1
                self.scis_win += 1
            elif computer_selection == SELECTION[ROCK]:
                print(f"{RULES[2]} (Rule #3) -> you lose!")
                self.comp_win += 1
                self.rock_win += 1

        elif user_selection == SELECTION[PAPER]:
            if computer_selection == SELECTION[SCISSORS]:
                print(f"{RULES[0]} (Rule #1) -> you lose!")
                self.comp_win += 1
                self.scis_win += 1
            elif computer_selection == SELECTION[ROCK]:
                print(f"{RULES[1]} (Rule #2) -> you win!")
                self.user_win += 1
                self.paper_win += 1

        elif user_selection == SELECTION[ROCK]:
            if computer_selection == SELECTION[SCISSORS]:
                print(f"{RULES[2]} (Rule #3) -> you win!")
                self.user_win += 1
                self.rock_win += 1
            elif computer_selection == SELECTION[PAPER]:
                print(f"{RULES[1]} (Rule #2) -> you lose!")
                self.comp_win += 1
                self.paper_win += 1

    def show_stats(self):
        """ Show some statistics """
        print(f"\nShowing some statistics.")
        print(f" # Games: {self.games_count}")
        print(f" You won: {self.user_win} Computer won: {self.comp_win} Ties: {self.tie}")
        print(f"Rock won: {self.rock_win} Scissors won: {self.scis_win} Paper won: {self.paper_win}")

    def play_game(self):
        """ Repeat the playing-game loop """
        user_selection: str = ""
        while user_selection != SELECTION[QUIT]:
            print(f"\n[#{self.games_count+1:02}] What do you want to play?")
            print("[R]ock [S]cissors [P]aper [Q]uit game")

            user_selection = self.get_user_choice()

            computer_selection = random.choice(SELECTION[ROCK:QUIT])

            if user_selection == SELECTION[UNKNOWN]:
                print("\nUnknown selection! Please type r/s/p or q.")
            elif user_selection != SELECTION[QUIT]:
                self.games_count += 1
                self.pick_winner(user_selection, computer_selection)


if __name__ == '__main__':
    say_hello()
    GAME = Game()
    GAME.play_game()
    GAME.show_stats()
    say_bye()
